# Vereinsdokumente

In diesem Repository sind die offiziellen Vereinsdokumente unseres Vereins Softwerke Magdeburg e. V.

Die aktuellen Vereinsdokuemte sind in den Releases zu finden.

## Latex

Da wir alle Dokumente auf Latex umgestellt haben, hier ein minimales Template zur Erstellung neuer Dokumente:

```tex
\documentclass[ 
%	bw % Farbige Elemente aus dem Template werden zu Schwarzweiß - bspw. das Logo 
]{Softwerke}

% Welchen Stand (hier zählt der Beschluss) hat das Dokument
\newcommand{\Stand}{16. April 2023} 

\begin{document}

\thispagestyle{plain}
\softwerkekopf

\section{Vereinssatzung} 
Loitsche, \Stand

\subsection{§ 1 .....}

% Hier kommt der Inhalt des Dokument hin, bitte nur subsection, subsubsection und paragraph als Ebenen verwenden. Sonst muss ggf. die Klasse angepasst werden.

\subsection{Änderungshistorie}
% Auch wenn die Dokumente im Git liegen und dadurch versioniert werden, sollte auch auf einem Ausdruck/PDF ersichtlich sein, wann das Dokument geändert wurde und durch welches Organ. 

% Unten auf der Letzten Seite steht die Erklärung zur Lizenz. Alle unsere Dokumente stehen unter einer CC-BY-NC-SA Lizenz
\vspace*{\fill}
\doclicenseThis
\end{document}

```

Da die Schriftarten direkt aus den TTF-Dateien geladen werden, muss zum übersetzen XeLaTeX verwendet werden!