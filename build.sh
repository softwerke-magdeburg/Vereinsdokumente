#!/bin/bash

# Check if xelatex is installed
if ! [ -x "$(command -v xelatex)" ]; then
  echo 'Error: xelatex is not installed.' >&2
  exit 1
fi

if [ -z "$1" ]; then
	echo "No file specified..."
	exit 1
fi

xelatex -file-line-error -interaction=errorstopmode -halt-on-error -output-directory=./.tmp $1

# Run twice for lastPage to work
echo "If the footer has ?? for the Number of pages, rerun build.sh"