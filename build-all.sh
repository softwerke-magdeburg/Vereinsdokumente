#!/bin/bash

# Check if xelatex is installed
if ! [ -x "$(command -v xelatex)" ]; then
  echo 'Error: xelatex is not installed.' >&2
  exit 1
fi

mkdir -p "./.tmp"

for f in ./*.tex
do
	echo "Info: Processing $f"
	xelatex -file-line-error -interaction=errorstopmode -halt-on-error -output-directory=./.tmp $f
	# Run twice for lastPage to work
	xelatex -file-line-error -interaction=errorstopmode -halt-on-error -output-directory=./.tmp $f
done

cp ./.tmp/*pdf .
