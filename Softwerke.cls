\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{Softwerke}[2024/03/28 Softwerke Dokumente Template]

% Alle Farbigen Inhalte Schwarz-Weiß machen (Logo, Farbwahl, ...)
\usepackage{etoolbox}
\newtoggle{printbw}

\DeclareOption{bw}{\toggletrue{printbw}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax

% Grundlage für die neue Klasse ist folgende Klasse
\LoadClass[11pt]{article}

% Geometrie der Seiten definieren
\usepackage{geometry}
\geometry{
a4paper,
left=30mm,
right=30mm,
top=30mm,
bottom=25mm,
head=14.5pt,
marginparwidth=20mm,
marginparsep=5mm}

% Softwerke-Farben definieren
\RequirePackage{xcolor}
\RequirePackage{xifthen}
\definecolor{SoftwerkeGruen}{HTML}{00ae7b}
\definecolor{SoftwerkeGruenDunkel}{HTML}{0f7d5d}
\definecolor{SoftwerkeGruenHell}{HTML}{a3d2b1}
\definecolor{SoftwerkeGrauDunkel}{HTML}{585758}
\definecolor{SoftwerkeGrauHell}{HTML}{d6d5d5}

% ToDo: Farben anpassen...


% Schriftarten laden und definieren
\usepackage[T1]{fontenc}
\usepackage{fontspec, titlesec}

\newfontfamily\alata{Alata}[
	Path = ./assets/Fonts/Alata/,
	Extension = .ttf,
    UprightFont = Alata-Regular
]
\setmainfont{BeVietnamPro}[
	Path = ./assets/Fonts/BeVietnamPro/,
	Extension = .ttf,
    UprightFont = BeVietnamPro-Regular,
    BoldFont = BeVietnamPro-Bold,
    ItalicFont = BeVietnamPro-Italic,
    BoldItalicFont = BeVietnamPro-BoldItalic
]

\newcommand{\alatafont}[2]{\alata\upshape\fontsize{#1}{#2}\selectfont}

% Schriftart mit Textsymbolen für bspw. Aufzählungen
%\usepackage{textcomp}

% Section in Alata
% Üblicherweise bei uns der Dokumententitel bspw. Vereinssatzung
\titleformat{\section}{\alatafont{25}{27}}{\thesection}{1em}{}

% Subsection und Subsubsection in Alata
% Üblicherweise bei uns Überschriften
\titleformat{\subsection}{\alatafont{15}{17}}{\thesubsection}{1em}{}
\titleformat{\subsubsection}{\alatafont{13}{14}}{\thesubsubsection}{1em}{}

% paragraph und subparagraph in Be Vietnam Pro
\titleformat{\paragraph}{\normalfont\bfseries\selectfont}{\theparagraph}{1em}{}
\titleformat{\subparagraph}{\normalfont\bfseries\selectfont}{\thesubparagraph}{1em}{}

% Ich weiß nicht mehr genau warum?
\parindent 0pt
\parskip 1.5ex plus 05ex minus 0.5ex

% Überschriften standardmäßig nicht nummerieren
\setcounter{secnumdepth}{0}

% Aufzählungen mit arabischer Nummerierung und kleinen Abständen
\usepackage{enumitem}
\def\labelenumi{\arabic{enumi}.}
\def\labelitemi{•}
\setlist{topsep=0cm}
\setlist{parsep=0cm}

\usepackage[ngerman]{babel}

% Füge Softwerke Logo in der oberen-rechten-Ecke ein auf der ersten Seite
\usepackage{graphicx}
\usepackage{xurl}
\usepackage{hyperref}
\newcommand{\softwerkekopf}{
\hfill
\begin{minipage}[t]{0.31\textwidth}
	\iftoggle{printbw}
	{\includegraphics[width=4.2cm]{assets/Hauptlogo/Schwarz.png}}
 	{\includegraphics[width=4.2cm]{assets/Hauptlogo/Gruen.png}}
 	\medbreak
  	\alatafont{10}{10}
 	Softwerke Magdeburg e.~ V.\\
	Stendaler Straße 4\\
	39326 Loitsche\smallbreak
	\href{mailto:kontakt@softwerke.md}{kontakt@softwerke.md}\\
	\href{https://www.softwerke.md}{www.softwerke.md}
\end{minipage}
\vspace{-2.5cm}
}

% Kopf und Fußzeilen konfigurieren
\usepackage{fancyhdr}
\usepackage[type={CC},modifier={by-nc-sa},version={4.0}]{doclicense}
\usepackage{lastpage}
% Standardformat
\pagestyle{fancy}
\fancyhf{}
\rhead{\alatafont{10}{10}\nouppercase{\leftmark}}
\lhead{\includegraphics[height=9pt]{assets/Sekundaerlogo/Schwarz.png}}
\lfoot{\alatafont{10}{10}Seite \thepage~von~\pageref*{LastPage}}
\cfoot{\doclicenseIcon}
\rfoot{\alatafont{10}{10}Stand: \Stand}
\renewcommand{\footrulewidth}{0.4pt}

\fancypagestyle{plain}{
	\fancyhf{}
	\lfoot{\alatafont{10}{10}Seite \thepage~von~\pageref*{LastPage}}
	\rfoot{\alatafont{10}{10}Stand: \Stand}
	\cfoot{\doclicenseIcon}
	\renewcommand{\headrulewidth}{0pt} 
}


% Block-Zitat formatieren
% Hauptsächlich für Beschlüsse genutzt
\usepackage{framed}
\usepackage{xcolor}
\let\oldquote=\quote
\let\endoldquote=\endquote
\colorlet{shadecolor}{SoftwerkeGruenHell}
\renewenvironment{quote}{\begin{shaded*}\begin{oldquote}\color{SoftwerkeGrauDunkel}}{\end{oldquote}\end{shaded*}}


% Weitere Bibliotheken
\usepackage{booktabs}
\usepackage{amssymb}
\usepackage{eurosym}