package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
	"time"
)

var markdowns = []string{"Beitragsordnung.md", "Datenschutz.md", "Geschäftsordnung.md", "Mission Statement.md", "Nutzungsbedingungen.md", "Vereinssatzung.md"}
var pdfs = []string{"Mitgliedsantrag.pdf"}

type File struct {
	Name string
	File string
	changeDate string
	changeCommit string
}


func main() {
	/// Create Release Message
	releaseFiles := []File{}
	for _, f := range markdowns {
		releaseFiles = append(releaseFiles, File{File: f, Name: strings.TrimSuffix(f, ".md"), changeDate: getChangeDate(f), changeCommit: getChangeCommit(f)})
	}
	for _, f := range pdfs {
		releaseFiles = append(releaseFiles, File{File: f, Name: strings.TrimSuffix(f, ".pdf"), changeDate: getChangeDate(f), changeCommit: getChangeCommit(f)})
	}
	releaseMsg := generateReleaseMessage(releaseFiles)
	fmt.Printf("Releasemessage will be:\n%s\n\n", releaseMsg)
	os.WriteFile("releaseMsg", []byte(releaseMsg), 0644)

	/// generate Pandoc Compile Script
	fmt.Println("Files added to compiling list:")
	compile := "#!/bin/sh\n\nmkdir -v output\n\n"
	for _, f := range markdowns {
		fmt.Printf("\t- %s\n", f)
		compile += exec.Command("echo", "Compiling file: ", f).String() + "\n"
		compile += exec.Command("pandoc", "-f", "markdown", "-t", "pdf", "-s", "\"" + f + "\"", "-o", "\"output/" + strings.TrimSuffix(f, ".md") + ".pdf\"", "--template=./templates/softwerke-template.latex", "--fail-if-warnings").String() + "\n"
	}
	//fmt.Println(compile)
	os.WriteFile("compile.sh", []byte(compile), 0744)
}

func getChangeDate(file string) string {
	out, _ := exec.Command("git", "log", "-1", "--date=format:%d.%m.%Y", "--pretty=format:%cd", file).Output()
	return string(out)
}

func getChangeCommit(file string) string {
	out, _ := exec.Command("git", "log", "-1", "--pretty=format:%h", file).Output()
	return string(out)
}

func generateReleaseMessage(files []File) string {
	commitHash, _ := exec.Command("git", "log", "-1", "--pretty=format:%h").Output()
	commitMsg, _ := exec.Command("git", "log", "-1", "--pretty=format:%s").Output()
	out := fmt.Sprintf("Alle Vereinsdokumente als PDF in der Lesefassung. Compiliert am %s.\n", time.Now().Format("02.01.2006"))
	out += fmt.Sprintf("Auslöser war der Commit %s mit der Commitmessage _%s_.\n\n", string(commitHash), string(commitMsg))
	out += "Die Dateien wurden automatisch von der CI gebaut."
	return out
}