# Protokoll der Mitgliedsversammlung

Dies ist die Niederschrift der ersten Mitgliedsversammlung des Vereins **Softwerke Magdeburg e. V.** (nachfolgend auch "die Softwerke").

Die Mitgliedsversammlung fand am 01.05.2022 in hybrider Form statt. Mitglieder konnten sich mittels einer Videokonferenz-Schaltung in die Präsenzveranstaltung einklinken. Der Veranstaltungsort boten die Räumlichkeiten der Werk- und Kulturscheune e. V. in Loitsche. Beginn der Mitgliedsversammlung war um 10:45 Uhr.

Anwesend waren die folgenden Mitglieder: Hannes K., Moritz, Ina, Hannes H., Alexa, Malik, Till, Phillipp

## Tagesordnung

 1. Begrüßung der Anwesenden
 2. Organisatorisches - How to MV
 3. Anträge an die Tagesordnung
 4. Bericht der Kassenprüfenden & des Kassenführenden
 5. Bericht des Vorstands
 6. Bits & Bäume Forderungen & Zweig
 7. Änderungen an Ordnungen
 8. Hochschulggruppe
 9. AG Übersetzung
10. Bestellung der Wahlleitung
11. Wahl des 1. Vorsitzes
12. Wahl des 2. Vorsitzes
13. Wahl der Kassenführung
14. Wahl der Kassenprüfenden

## TOP 1: Begrüßung der Anwesenden

Till begrüßt die Anwesenden Mitglieder zur ersten Mitgliedsversammlung.

## TOP 2: Organisatorisches - How to MV

* Die Sitzungsleitung wird durch den Vorstand, vertreten durch Till, übernommen.
* Phillipp erklärt sich bereit, das Protokoll zu schreiben. Zu protokollieren sind nach der Satzung §13 Abs. 13. Beschlüsse und Wahlen der Mitgliedsversammlung.
* Die Beschlussfähigkeit ist nach §8 der Satzung gegeben.
* Sitzungsfomalia:
  * Wortmeldungen bitte durch Handzeichen und Till erteilt das Wort
  * Wahlen sind grundsätzlich offen, wenn eine Person geheime Wahlen fordert, kann dies vor der Wahl geschehen. §13 Abs. 12. Satzung

## TOP 3: Anträge an die Tagesordnung

Es gibt 3 Anträge zur Änderung der Tagesordnung. 

Da zuerst die Empfehlung der Kassenprüfung gehört werden soll, wird beantrag die Tagesordnungspunkte 4 und 5 zu tauschen.

Die Tagesordung soll um die Tagesordnungspunkte 8 "Gründungg einer Hochschulgruppe" und 9 "AG Übersetzung" erweitert werden. Nachfolgende Punkte verschieben sich entsprechend.

> Die Mitgliedsvesammlung beschließt die Tagesordnungspunkte TOP 8 "Gründung einer Hochschulgruppe" und TOP 9 "AG Übersetzung" zu ergänzen. Und Top 4 und 5 zu tauschen.
>
> Dafür: 9, Enthaltung: 0, Dagegen: 0 → Angenommen

## TOP 4: Bericht der Kassenprüfenden & des Kassenführenden

Die Kassenprüfung fand am 29.04.2022 statt und es gab keine Beanstandungen. Das Protokoll der Prüfung sowie der Kassenbericht lag der Versammlung zur Einsicht vor. Die Kassenprüfenden empfehlen die Entlastung der Kassenführung.

Die Kassenführung fasst die Zahlen des Jahres 2021 wie folgt zusammen: Die Einnahmen setzen sich zusammen aus 553,00 € Mitgliedsbeiträgen und 19,00 € Spenden. Ausgaben gab es 75,00 € für die Eintragung im Vereinsregister, 1,84 € an die Triodos Bank für die Kontoführung und 196,82 € für den Betrieb unserer IT-Infrastruktur.

> Die Kassenprüfung entlastet die Kassenführung hinsichtlich dieser und empfiehlt die Entlastung des Vorstands.

## TOP 5: Bericht des Vorstands

Der Vorstand berichtet über die Ereignisse seit der Gründung. Unter anderem hat sich der Verein von 7 Gründungsmitgliedern auf 10 Mitglieder erweitert. Wir waren vertreten im Podcast "digital leben" des MDR und haben einen Workshop durchgeführt und einen weiteren in Planung. Unser Diensteangebot hat sich über das Jahr erweitert und diese werden auch von einigen Menschen genutzt. 

Der Vorstand stimmt bei diesem Beschluss nicht mit ab.

> Die Mitgliedsversammlung beschließt den Vorstand (Till-Frederik Riechard, Malik Mann und Phillipp Engelke) und für die vergangene Legislatur zu entlasten.
>
> Dafür: 6, Enhaltungen: 0, Dagegen: 0 → Angenommen

## TOP 6: Bits & Bäume Forderungen & Zweig

Bits & Bäume wird grundlegend vorgestellt.

> Die Mitgliedsversammlung beschließt die Forderungen der Bits & Bäume Bewegung (siehe <https://bits-und-baeume.org/forderungen/info/de>)zu unterzeichnen und die Handlungen des Vereins in Zukunft auch an diesen auszurichten.
>
> Dafür: 9, Enhaltungen: 0, Dagegen: 0 → Angenommen

> Die Mitgliedsversammlung beauftragt den Vorstand, die Bedingungen eines Bits & Bäume Zweiges (<https://bits-und-baeume.org/regionalzweige/de>) zu prüfen und gegebenenfalls die Umsetzung dessen anzustoßen.
>
> Dafür: 9, Enhaltungen: 0, Dagegen: 0 → Angenommen

## TOP 7: Änderungen an Ordnungen

Es ist noch ein Pull-Request zur Änderung der Beitragsordnung offen. Dieser fügt die Zahlungmöglichkeit per SEPA-Lastschrift hinzu. Auf der Versammlung wird der Satz nochmals angepasst um Daueraufträge nicht auszuschließen.

> Die Mitgliedsversammlung beschließt die Änderung an der Beitragsordnung wie in Pull-Request 7 auf <https://codeberg.org/softwerke-magdeburg/Vereinsdokumente> hinterlegt.
>
> Dafür: 9, Enhaltungen: 0, Dagegen: 0 → Angenommen

## TOP 8: Hochschulggruppe

Die Möglichkeiten und Verpflichtungen einer Hochschulgruppe an der Otto-von-Guericke Universität Magdeburg werden vorgestellt und diskutiert.

> Die Mitgliedsversammlung beauftragt den Vorstand die Gründung einer Hochschulgruppe zu beantragen.
>
> Dafür: 9, Enhaltungen: 0, Dagegen: 0 --> Angenommen

## TOP 9: AG Übersetzung

Es hatte bereits eine Arbeitssession zur Übersetzung der Webseite stattgefunden. Um auch weiterhin einen Ansprechpartner und Helfer für die Übersetzung der Inhalte zu haben, soll eine Arbeitsgruppe gegründet werden.

Der Name AG Übersetzung trifft nicht auf Zustimmung und die Mitgliedsversammlung spricht sich für den Namen "AG Sprache" aus.

Für die Einberufung von AGs ist der Vorstand zuständig, sodass diese Aufgabe auf die nächste Vorstandssitzung vertagt wird. Alexa hat sich bereits als Ansprechperson angeboten.

## TOP 10: Bestellung der Wahlleitung

> Die Versammlungsleitung bestellt Ina Stausebach zur Wahlleitung.

Die Wahlleitung darf nicht zur Wahl stehen, wenn sie sich zur Wahl aufstellen lassen möchte, kann die Wahlleitung in dieser Wahl vertreten werden.

## TOP 11: Wahl des 1. Vorsitzes

Für die Position des 1. Vorsitzes wird Till-Frederik Riechard vorgeschlagen. Er nimmt den Vorschlag an.

> Die Mitgliedsversammlung wählt Till-Frederik Riechard mit 8 Stimmen dafür, einer enthaltung und keiner Gegenstimme. Er nimmt die Wahl an.

## TOP 12: Wahl des 2. Vorsitzes

Für die Position des 2. Vorsitzes wird Malik Mann vorgeschlagen. Er nimmt den Vorschlag an.

> Die Mitgliedsversammlung wählt Malik Mann mit 8 Stimmen dafür, einer enthaltung und keiner Gegenstimme. Er nimmt die Wahl an.

## TOP 13: Wahl der Kassenführung

Für die Position der Kassenführung wird Phillipp Engelke vorgeschlagen. Er nimmt den Vorschlag an.

> Die Mitgliedsversammlung wählt Phillipp Engelke mit 8 Stimmen dafür, einer enthaltung und keiner Gegenstimme. Er nimmt die Wahl an.

## TOP 14: Wahl der Kassenprüfenden

Die Wahlleitung wird in dieser Wahl durch Till übernommen.

Vorgeschlagen werden Moritz Marquardt, Ina Stausebach und Alexandra Grube. Alle drei akzeptieren den Vorschlag.

> Die Mitgliedsversammlung wählt Moritz Marquardt, Ina Stausebach und Alexandra Grube einstimmig mit 9 Stimmen als Kassenprüfende.

\vspace{5em}
\begin{tabular}{@{}p{.37\textwidth}p{.6\textwidth}@{}}
    \\\\
    \hrulefill & \hrulefill \\
    Ort, Datum & Sitzungsleitung: Till-Frederik Riechard
\end{tabular}