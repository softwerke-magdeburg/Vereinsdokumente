# Protokoll der Mitgliedsversammlung

Dies ist die Niederschrift der zweiten Mitgliedsversammlung des Vereins **Softwerke Magdeburg e. V.** (nachfolgend auch "die Softwerke"). 

Die Mitgliedsversammlung fand am 16.04.2023 in hybrider Form statt. Mitglieder konnten sich mittels einer Videokonferenz-Schaltung in die Präsenzveranstaltung einklinken. Der Veranstaltungsort boten die Räumlichkeiten der Werk- und Kulturscheune e. V. in Loitsche. Beginn der Mitgliedsversammlung war um **10:10** Uhr.  Anwesend waren die folgenden Mitglieder: Till, Ina, Malik, Moritz, Hannes K., Phillipp, Johannes und Hannes H. (digital).

## Tagesordnung

 1. Begrüßung der Anwesenden
 2. Organisatorisches - How to MV
 3. Anträge an die Tagesordnung
 4. Bericht der Kassenprüfenden & des Kassenführenden
 5. Bericht des Vorstands
 6. Änderungen an Satzung & Ordnungen
 7. Bestellung der Wahlleitung
 8. Wahl des 1. Vorsitzes
 9. Wahl des 2. Vorsitzes
10. Wahl der Kassenführung
11. Wahl der Kassenprüfenden
12. Beschluss des Protokolls

## TOP 1: Begrüßung der Anwesenden

Till begrüßt die anwesenden Mitglieder zur zweiten Mitgliedsversammlung und stellt die Tagesordnung vor.

## TOP 2: Organisatorisches – How to MV

* Die Sitzungsleitung wird durch den Vorstand, vertreten durch Till, übernommen.
* Phillipp erklärt sich bereit, das Protokoll zu schreiben. Zu protokollieren sind nach der Satzung § 13 Abs. 13 Beschlüsse und Wahlen der Mitgliedsversammlung.
* Die Beschlussfähigkeit ist nach § 8 der Satzung gegeben.
* Sitzungsformalia: 
  * Wortmeldungen bitte durch Handzeichen und Till erteilt das Wort
  * Wahlen sind grundsätzlich offen; wenn eine Person geheime Wahlen fordert, kann dies vor der Wahl geschehen (§ 13 Abs. 12. Satzung).

## TOP 3: Anträge an die Tagesordnung

1. Till: Anpassung § 9 Abs. 3 der Satzung
2. Phillipp: Anpassung § 14 Abs. 2 der Satzung
3. Till: Anpassung § 11 Abs. 3 der Satzung
4. Phillipp: Anpassung § 13 Abs. 6 der Satzung
5. Till: Anpassung der Satzung bzgl. neuem Versuch die Gemeinnützigkeit zu erlangen
6. Till: Korrektur der Rechtschreibung und Gramatik in Satzung und Ordnungen 

> Die Mitgliederversammlung beschließt die obige Tagesordnung. Diese wird durch die genannten 6 Anträge unter TOP 6: Änderungen an Satzung & Ordnungen ergänzt. **8** Dafür, 0 Dagegen, 0 Enthaltung → **Angenommen**

## TOP 4: Bericht der Kassenprüfenden & des Kassenführenden

Die Kassenprüfung fand am 02.04.2023 statt und es gab keine Beanstandungen. Das Protokoll der Prüfung sowie der Kassenbericht lag der Versammlung zur Einsicht vor. **Die Kassenprüfenden empfehlen die Entlastung der Kassenführung.**

Die Kassenführung fasst die Zahlen des Jahres 2023 wie folgt zusammen: Die Einnahmen setzen sich zusammen aus **745,00** € Mitgliedsbeiträgen, **734,00** € Spenden und **200,00** € aus Kooperation. Ausgaben gab es **303,35** € für unser Vereinswochenende, **6,50** € an die Triodos Bank für die Kontoführung und **599,42** € für den Betrieb unserer IT-Infrastruktur.

**→  Die Kassenprüfung entlastet die Kassenführung hinsichtlich dieser und empfiehlt die Entlastung des Vorstands.**

Finanzprognose 2023:

* Mehrkosten für IT-Ausgaben im Vergleich zum Vorjahr
* relevante Ausgaben für Vereinswochenende, Wandercoaching und CI-Entwicklung
* Kostendeckung für den IT-Betrieb und unsere Bildungsarbeit aktuell gesichert, für Aktionen und Entwicklung des Vereins und unseres Angebots, sind wir jedoch auf Spenden angewiesen

## TOP 5: Bericht und Entlastung des Vorstands

Der Vorstand berichtet über die Ereignisse seit der letzten Mitgliederversammlung.

* Wir haben (Stand heute) 13 Mitglieder. Steigerung der Mitgliederzahlen seit letztem Jahr.
* 140 registrierte Accounts.
* Mastodon Nutzer 861 Accounts, davon 290 aktive Accounts.

Der Vorstand stimmt bei diesem Beschluss nicht mit ab (→ 5 Stimmberechtigte hier).

> Die Mitgliedsversammlung beschließt den Vorstand (Till-Frederik Riechard, Malik Mann und Phillipp Engelke) und für die vergangene Legislatur zu entlasten.
>
> Dafür: **5**, Enthaltungen: **0**, Dagegen: **0** → **Angenommen**

Der Vorstand wurde erfolgreich entlastet.

## TOP 6: Änderungen an Satzung und Ordnungen

### TOP 6.1: Anpassung der Rechtschreibung

> Die Mitgliederversammlung beschließt die Änderungen der Satzung nach <https://codeberg.org/softwerke-magdeburg/Vereinsdokumente/pulls/14>.
>
> Dafür: **6**, Enthaltungen: **1**, Dagegen: **0** → **Angenommen**

*Nachtrag 19.04.2023: PR in Satzung v2 gemerged*

### TOP 6.2: Anpassung § 9 Abs. 3 - AG Ansprechpersonen

Der neue Wortlaut soll heißen:

*Der Vorstand kann zweckgebundene Arbeitsgruppen bilden, mit konkreten Entscheidungsbefugnissen ausstatten und auflösen. Jede Arbeitsgruppe bestimmt eine Ansprechperson, die gegenüber dem Vorstand benannt wird. Arbeitsgruppen bilden kein eigenes Vereinsorgan.*


Die genauere Spezifikation der Veröffentlichung und Rechte und Pflichten der AG Ansprechpersonen soll in einer Ordnung spezifiziert werden. Diese Spezifizierung wird dem Vorstand aufgetragen. 

> Die Mitgliederversammlung beschließt die Änderung des § 9 Abs. 3 der Satzung, durch die Austausch des Absatzes mit der oben genannten Formulierung.
>
> Dafür: **7**, Enthaltungen: **0**, Dagegen: **1** → **Angenommen**

*Nachtrag 19.04.2023: PR in Satzung v2 gemerged aus* <https://codeberg.org/softwerke-magdeburg/Vereinsdokumente/pulls/21>

### TOP 6.3: Anpassung § 14 - Kassenprüfung

Anpassung:

* Entlastung des Vorstandes hinsichtlich der Kassenführung – nicht nur der kassenführenden Person
* sprechen eine Empfehlung aus – nicht empfehlen (da dies eine notwendige positive Empfehlung impliziert)

> Die Mitgliederversammlung beschließt die Änderungen der Satzung nach <https://codeberg.org/softwerke-magdeburg/Vereinsdokumente/pulls/17>.
>
> Dafür: **8**, Enthaltungen: **0**, Dagegen: **0** → **Angenommen**

*Nachtrag 19.04.2023: PR in Satzung v2 gemerged*

### TOP 6.4: Anpassung § 11 - Amtszeit Vorstand

Anpassung:

* nach Ausscheiden eines Vorstands ist der Ersatz durch die MV zu bestimmen und nicht durch den restlichen Vorstand

> Die Mitgliederversammlung beschließt die Änderungen der Satzung nach <https://codeberg.org/softwerke-magdeburg/Vereinsdokumente/pulls/18>.
>
> Dafür: **8**, Enthaltungen: **0**, Dagegen: **0** → **Angenommen**

*Nachtrag 19.04.2023: PR in Satzung v2 gemerged*

### TOP 6.5: Anpassung § 13 - Entlastung des Vorstands

> Die Mitgliederversammlung beschließt die Änderungen der Satzung nach <https://codeberg.org/softwerke-magdeburg/Vereinsdokumente/pulls/20>.
>
> Dafür: **8**, Enthaltungen: **0**, Dagegen: **0** → **Angenommen**

*Nachtrag 19.04.2023: PR in Satzung v2 gemerged*

### TOP 6.6 Anpassung für die Eintragung

Der PR beinhaltet:

* Zusammenfassung in § 1
* Anpassung hinsichtlich des Anstrebens der Gemeinnützigkeit
* Hinzufügen einer Änderungshistorie

> Die Mitgliederversammlung beschließt die Änderungen der Satzung nach <https://codeberg.org/softwerke-magdeburg/Vereinsdokumente/pulls/11>.
>
> Dafür: **8**, Enthaltungen: **0**, Dagegen: **0** → **Angenommen**

*Nachtrag 19.04.2023: PR in Satzung v2 gemerged*

## TOP 7: Bestellung der Wahlleitung

> Die Versammlungsleitung bestellt **Johannes** zur Wahlleitung.

Die Wahlleitung darf nicht zur Wahl stehen; wenn sie sich zur Wahl aufstellen lassen möchte, kann die Wahlleitung in dieser Wahl vertreten werden.

Ablauf:

* Vorschläge sammeln
* Vorgeschlagene fragen, ob sie antreten möchten
* Abstimmung mit Ja/Nein/Enthaltung bezüglich der Wahl einer Person
* Fragen, ob die Person die Wahl annimmt

## TOP 8: Wahl des 1. Vorsitzes

Für die Position des 1. Vorsitzes werden **Till**, **Phillipp**, **Moritz**, **Hannes K.** vorgeschlagen. Till nimmt den Vorschlag an, alle anderen lehnen ab.

> Die Mitgliedsversammlung wählt **Till** mit **7** Stimme(n) dafür, **1** Enthaltung(en) und **0** Gegenstimme(n) zum 1. Vorstandsvorsitz.
>
> **Till** nimmt die Wahl an.

## TOP 9: Wahl des 2. Vorsitzes

Für die Position des 2. Vorsitzes werden **Hannes K.**, **Ina**, **Malik** vorgeschlagen. Ina, Malik und Hannes K. nehmen den Vorschlag an.

> Die Mitgliedsversammlung stimmt für **Ina** mit **3** Stimmen, für **Malik** mit **0** Stimmen, für **Hannes K.** mit **5** Stimmen nach einfacher Mehrheitswahl. Es gibt **0** Enthaltungen.
>
> **Hannes K.** nimmt die Wahl an.

## TOP 10: Wahl der Kassenführung

Für die Position der Kassenführung werden **Phillipp**, **Ina** vorgeschlagen. Beide nehmen den Vorschlag an.

> Die Mitgliedsversammlung stimmt für **Ina** mit **3** Stimmen, für **Phillipp** mit **4** Stimmen nach einfacher Mehrheitswahl. Es gibt **1** Enthaltungen.
>
> **Phillipp** nimmt die Wahl an.

## TOP 11: Wahl der Kassenprüfenden

In diesem Punkt wird die Wahlleitung durch Till übernommen.

Vorgeschlagen werden **Ina**, **Moritz**, **Malik**, **Hannes H**, **Johannes**. Hannes H. lehnt ab, die anderen nehmen den Vorschlag an.

Da zwei oder mehr Personen diesen Posten bekleiden können, wird über jede Person einzeln abgestimmt.

> Die Mitgliedsversammlung wählt
>
> Ina: Dafür: **7**, Enthaltungen: **0**, Dagegen: **1** → **Sie nimmt die Wahl an**
>
> Johannes: Dafür: **7**, Enthaltungen: **0**, Dagegen: **1** → **Er nimmt die Wahl an**
>
> Moritz: Dafür: **8**, Enthaltungen: **0**, Dagegen: **0** → **Er nimmt die Wahl an**
>
> Malik: Dafür: **7**, Enthaltungen: **0**, Dagegen: **1** → **Er nimmt die Wahl an**
>
> als Kassenprüfende.

## TOP 12: Beschluss des Protokolls

> Die Mitgliederversammlung beschließt das Protokoll dieser Mitgliederversammlung. Redaktionelle Änderungen des Protokolls können im Nachgang noch erfolgen, insbesondere zur Dokumentation und Verbesserung des Verständnisses und der Lesbarkeit.
>
> Das Protokoll wird im Anschluss sowohl als Text als auch als Druckdatei (PDF) öffentlich im Repository unter <https://codeberg.org/softwerke-magdeburg/Vereinsdokumente/> bereitgestellt.
>
> Dafür: **8**, Enthaltungen: **0**, Dagegen: **0** → **Angenommen**

\vspace{5em}
\begin{tabular}{@{}p{.37\textwidth}p{.6\textwidth}@{}}
\\ \\
\hrulefill & \hrulefill \\
Ort, Datum & Sitzungsleitung: Till-Frederik Riechard
\end{tabular}

\vspace{5em}
\begin{tabular}{@{}p{.37\textwidth}p{.6\textwidth}@{}}
\\ \\
\hrulefill & \hrulefill \\
Ort, Datum & Protokoll: Phillipp Engelke
\end{tabular}

