# Protokoll der Mitgliedsversammlung

Dies ist die Niederschrift der dritten Mitgliedsversammlung des Vereins **Softwerke Magdeburg e. V.** (nachfolgend auch „die Softwerke“).

Die Mitgliedsversammlung fand am 21.07.2024 in hybrider Form statt. Mitglieder konnten sich mittels einer Videokonferenz-Schaltung in die Präsenzveranstaltung einklinken. Den Veranstaltungsort boten die Räumlichkeiten der Werk- und Kulturscheune e. V. in Loitsche. Beginn der Mitgliedsversammlung war um **09:40** Uhr. Ende der Sitzung war um **12:03** Uhr.

Anwesend waren die folgenden Mitglieder: Till (Till-Frederik), Ulli (Christian), Johannes, Hannes (Hannes K.), Moritz, Achim (Hans-Joachim), Ina, Phillipp

## Tagesordnung 

*nach ursprünglicher Einladung*

TOP 0: Begrüßung der Anwesenden  
TOP 1: Organisatorisches – How to MV  
TOP 2: Anträge an die Tagesordnung  
TOP 3: Bericht der Kassenprüfenden & des Kassenführenden  
TOP 4: Bericht des Vorstands   
TOP 5: Neufassung von Satzung & Ordnungen   
TOP 6: Bestellung der Wahlleitung  
TOP 7: Wahl des 1. Vorsitzes  
TOP 8: Wahl des 2. Vorsitzes  
TOP 9: Wahl der Kassenführung  
TOP 10: Wahl der Kassenprüfenden  
TOP 11: Beschluss des Protokolls

## TOP 0: Begrüßung der Anwesenden

Till begrüßt die anwesenden Mitglieder zur dritten Mitgliedsversammlung und stellt die Tagesordnung vor.

## TOP 1: Organisatorisches – How to MV

* Die Sitzungsleitung wird durch den Vorstand, vertreten durch Till, übernommen.
* Phillipp erklärt sich bereit, das Protokoll zu schreiben. Zu protokollieren sind nach der Satzung § 13 Abs. 13 Beschlüsse und Wahlen der Mitgliedsversammlung.  
* Die Beschlussfähigkeit ist nach § 8 der Satzung gegeben.  
* Sitzungsformalia: 
  * Wortmeldungen bitte durch Handzeichen und Till erteilt das Wort
  * Wahlen sind grundsätzlich offen; wenn eine Person geheime Wahlen fordert, kann dies vor der Wahl geschehen (§ 13 Abs. 12. Satzung).  

## TOP 2: Anträge an die Tagesordnung

- TOP 5.1 hinzufügen: Diskussion und Grundsatzentscheidung Gemeinnützigkeit – Till/Phillipp
- TOP 3 & TOP 4 tauschen, sodass erst der Vorstandsbericht kommt, dann in den Bericht der Kasse übergeht – Phillipp
- TOP 5.2 hinzufügen: Anträge an die Tagesordnung der Mitgliederversammlung von der Satzung (§13) in die Geschäftsordnung verschieben und die Frist anpassen – Till

> Die Mitgliederversammlung beschließt die obige Tagesordnung. Diese wird durch die genannten 2 Anträge unter TOP 5: Neufassung von Satzung & Ordnungen ergänzt. Außerdem werden TOP 3 und 4 getauscht  
> Dafür: 7, Dagegen: 0, Enthaltung: 1 → **Angenommen**

## beschlossene Tagesordnung

TOP 0: Begrüßung der Anwesenden  
TOP 1: Organisatorisches – How to MV  
TOP 2: Anträge an die Tagesordnung  
TOP 3: Bericht des Vorstands  
TOP 4: Bericht der Kassenprüfenden & des Kassenführenden  
TOP 5: Neufassung von Satzung & Ordnungen  
TOP 5.1: Diskussion und Grundsatzentscheidung Gemeinnützigkeit  
TOP 5.2: Anträge an die Tagesordnung der Mitgliederversammlung von der Satzung (§13) in die Geschäftsordnung verschieben und die Frist anpassen  
TOP 5.3: Beschluss der Neufassung der Satzung  
TOP 6: Bestellung der Wahlleitung  
TOP 7: Wahl des 1. Vorsitzes  
TOP 8: Wahl des 2. Vorsitzes  
TOP 9: Wahl der Kassenführung  
TOP 10: Wahl der Kassenprüfenden  
TOP 11: Beschluss des Protokolls

## TOP 3: Bericht des Vorstands

Der Vorstand berichtet über die Ereignisse seit der letzten Mitgliederversammlung:

- Wir haben leider noch nicht die Gemeinnützigkeit erlangt. Siehe auch späterer TOP
- sind jetzt 22 Mitglieder (+9 zur letzten MV)
- im SSO 392 Accounts – wie viele davon regelmäßig aktiv sind, wissen wir nicht
- Mastodon 924 Accounts, davon 183 in den letzten 4 Wochen aktiv
- haben jetzt ein Logo und eine CI – Danke an die TF CI und Nany
- für AGs haben wir Budgets eingeführt, sodass hier Ausgaben basierend auf diesem Budget arbeiten können
- Bei der LNDW haben wir teilgenommen mit einem Stand.
- Wir haben einen Workshop durchgeführt
- Im Herbst haben wir an einem WanderCoaching teilgenommen, um u. a. unsere Mitgliederaquise zu verbessern.   
- neues Veranstalltungsformat Handystammtisch (Stendal)
- Haben 3000 € Preisgeld beim Kompass-Wettbewerb gewonnen  
- sind im Selbstverständnis und Rollendefinitionen weiterkommen

## TOP 4: Bericht der Kassenprüfenden & des Kassenführenden

Die Kassenprüfung fand am 27.06.2024 statt und es gab kleinere Beanstandungen, welche Nachgearbeitet wurden. Der Kassenbericht mit Unterschriften der Kassenprüfung lag der Versammlung zur Einsicht vor. **Die Kassenprüfenden empfehlen die Entlastung der Kassenführung.**

Die Kassenführung fasst die Zahlen des Jahres 2023 wie folgt zusammen: Die Einnahmen setzen sich zusammen aus **745,00** € Mitgliedsbeiträgen, **734,00** € Spenden und **200,00** € aus Kooperation. Ausgaben gab es **303,35** € für unser Vereinswochenende, **6,50** € an die Triodos Bank für die Kontoführung und **599,42** € für den Betrieb unserer IT-Infrastruktur.

**→  Die Kassenprüfung entlastet die Kassenführung hinsichtlich dieser und empfiehlt die Entlastung des Vorstands.**

Finanzprognose 2024:

* Mehrkosten für IT-Ausgaben im Vergleich zum Vorjahr
* Vereinswochenende dieses Jahr deutlich teurer
* Kostendeckung für den IT-Betrieb und unsere Bildungsarbeit aktuell gesichert, für Aktionen und Entwicklung des Vereins und unseres Angebots, sind wir jedoch auf Spenden angewiesen
* Ohne das Preisgeld wären wir dieses Jahr nach aktuellem Stand in einem leichten Minus für das Jahr, da CI und Vereinswochenende

Der Vorstand stimmt bei diesem Beschluss nicht mit ab (→ 5 Stimmberechtigte anwesend).

> Die Mitgliedsversammlung beschließt, den Vorstand (Till-Frederik Riechard, Hannes Kuehn und Phillipp Engelke) für die vergangene Legislatur zu entlasten.
>
> Dafür: **5**, Enthaltungen: **0**, Dagegen: **0** → **Angenommen**

Der Vorstand wurde erfolgreich entlastet.

## TOP 5: Neufassung von Satzung & Ordnungen

### TOP 5.1: Diskussion und Grundsatzentscheidung bezüglich der Gemeinnützigkeit

- Wir versuchen das schon sehr lange 


- Die Liste der möglichen gemeinnützigen Zwecke ist abgeschlossen und begrenzt
- Unsere Chancen auf Gemeinnützigkeit hängen von der Auslegung des Finanzamts ab, somit ist ein Vergleich mit anderen gemeinnützigen Vereinen in anderen Finanzamtsbezirken nur bedingt möglich
- Gemeinnützigkeit bedeutet auch, dass jede Ausgabe mit dem Vereinszweck (also der Gemeinnützigkeit) begründbar sein muss
- Finanzamt hat sich bisher vor allem am Bereitstellen von Diensten („Dienstleistung“) gestört
- Wollen wir unsere Satzung anpassen und mit einem restriktiveren Vereinszweck in der Satzung weiterarbeiten?
- Ergebnis der Diskussion:
  - Das Erreichen der Gemeinnützigkeit ist zeitlich auf jeden Fall aufwendig
  - Aktuell sind wir aufgrund der steuerlichen Aspekte nicht direkt benachteiligt
  - Mitglieder und Spendende können ihre Zuwendungen steuerlich geltend machen
  - Unsere Dienste anbieten muss dem Vereinszweck zuträglich sein, wir wollen uns hier nicht verbiegen
  - Wir sollten dem Finanzamt erklären, dass wir kein Produkt verkaufen, keine Gewährleistung geben.
  - Wir könnten uns rechtliche Informationen von einem Anwalt holen
  - Ist das Nutzen der Dienste über „Workshops“ dann möglich und einfach vermarktbar gegenüber dem Finanzamt

> Die Mitgliederversammlung beschließt als Grundsatzentscheidung bzgl. der Gemeinnützigkeit, dass wir die Erreichung der Gemeinnützigkeit weiter verfolgen möchten. Jedoch dürfen hierfür keine erheblichen Vereinsgelder ausgegeben werden. Die erarbeiteten Satzungsänderungen sollten die aktuellen Vereinszwecke unterstützen. Bei Zeitknappheit sollte die Ressource Zeit eher für die Vereinszwecke als für die Anstrebung der Gemeinnützigkeit genutzt werden.
>
> Dafür: **8**, Enthaltungen: **0**, Dagegen: **0** → **Angenommen**

Wir machen eine TF für die Gemeinnützigkeit auf, Ulli macht den Lead.

### TOP 5.2: Anträge an die Tagesordnung der Mitgliederversammlung von der Satzung (§13) in die Geschäftsordnung verschieben und die Frist anpassen

- In der Geschäftsordnung ergänzen:
  - § 5 Mitgliederversammlung   
    1. Aktive Mitglieder können Anträge für die Tagesordnung der Mitgliederversammlung einreichen.
    2. Anträge an die Tagesordnung sind vor der Mitgliederversammlung schriftlich an den Vorstand zu stellen. Zusätzlich können Anträge während der Mitgliederversammlung vor dem Beschluss der Tagesordnung auch mündlich eingebracht werden. 
    3. Der Vorstand kann in der Einladung zur Mitgliederversammlung eine Frist zur Einreichung von Anträgen an die Tagesordnung festlegen. Die Frist darf maximal 3 Tage vor der Mitgliederversammlung enden. 
- In der Satzung entfernen 
  - § 13 Mitgliederversammlung  

    9\. Anträge an die Tagesordnung sind spätestens 3 Tage vor der Mitgliederversammlung an den Vorstand zu stellen. 

<https://codeberg.org/softwerke-magdeburg/Vereinsdokumente/pulls/25> 

> Die Mitgliederversammlung beschließt, den § 13 Mitgliederversammlung Abs. 9 aus der Satzung zu entfernen. Außerdem wird die Geschäftsordnung um einen § 5 Mitgliederversammlung mit dem Absatz 1 "Aktive Mitglieder können Anträge für die Tagesordnung der Mitgliederversammlung einreichen.", dem Absatz 2 "Anträge an die Tagesordnung sind vor der Mitgliederversammlung schriftlich an den Vorstand zu stellen. Zusätzlich können Anträge während der Mitgliederversammlung vor dem Beschluss der Tagesordnung auch mündlich eingebracht werden." und dem Absatz 3 "Der Vorstand kann in der Einladung zur Mitgliederversammlung eine Frist zur Einreichung von Anträgen an die Tagesordnung festlegen. Die Frist darf maximal 3 Tage vor der Mitgliederversammlung enden." engänzt.
>
> Dafür: **8**, Enthaltungen: **0**, Dagegen: **0** → **Angenommen**

*Nachtrag: Der PR wurde am 30.07.2024 gemerged*

### TOP 5.3: Beschluss der Neufassung der Satzung

> Die Mitgliederversammlung beschließt die Satzung in der angepassten Form als Neufassung. Die Neufassung der Satzung ist dem Protokoll beigefügt. 
>
> Dafür: **8**, Enthaltungen: **0**, Dagegen: **0** → **Angenommen** 

## TOP 6: Bestellung der Wahlleitung

> Die Versammlungsleitung bestellt **Moritz** zur Wahlleitung.

Die Wahlleitung darf nicht zur Wahl stehen; wenn sie sich zur Wahl aufstellen lassen möchte, kann die Wahlleitung in dieser Wahl vertreten werden.

Ablauf:

* Vorschläge sammeln
* Vorgeschlagene fragen, ob sie antreten möchten
* Abstimmung mit Ja/Nein/Enthaltung bezüglich der Wahl einer Person
* Fragen, ob die Person die Wahl annimmt

## TOP 7: Wahl des 1. Vorsitzes

Für die Position des 1. Vorsitzes werden **Till und Hannes** vorgeschlagen. Till nimmt den Vorschlag an, Hannes lehnt ab.

> Die Mitgliedsversammlung wählt **Till** mit **7** Stimme(n) dafür, **1** Enthaltung(en) und **0** Gegenstimme(n) zum 1. Vorstandsvorsitz.
>
> **Till** nimmt die Wahl an.

## TOP 8: Wahl des 2. Vorsitzes

Für die Position des 2. Vorsitzes werden **Hannes, Ina, Phillipp, Johannes und Achim** vorgeschlagen. Hannes, Ina und Phillipp nehmen den Vorschlag an, Achim und Johannes lehnen ab.

Aussprache über die Nominierten

> Die Mitgliedsversammlung stimmt für **Hannes** mit **6** Stimmen, für **Phillipp** mit **0** Stimmen, für **Ina** mit **1** Stimmen nach einfacher Mehrheitswahl. Es gibt **1** Enthaltungen.
>
> **Hannes** nimmt die Wahl an.

## TOP 9: Wahl der Kassenführung

Für die Position der Kassenführung werden **Phillipp und Ina** vorgeschlagen. Phillipp nimmt den Vorschlag an, Ina lehnt ab.

> Die Mitgliedsversammlung wählt Phillipp mit **7** Stimme(n) dafür, **1** Enthaltung(en) und **0** Gegenstimme(n) zur Kassenführung.
>
> **Phillipp** nimmt die Wahl an.

## TOP 10: Wahl der Kassenprüfenden

Vorgeschlagen werden **Ina, Johannes, Ulli, Achim und Moritz**. Moritz lehnt ab, die anderen nehmen den Vorschlag an.

Da zwei oder mehr Personen diesen Posten bekleiden können, wird über jede Person einzeln abgestimmt.

> Die Mitgliedsversammlung wählt
>
> Ina: Dafür: **8**, Enthaltungen: **0**, Dagegen: **0** → **Sie nimmt die Wahl an**
>
> Johannes: Dafür: **7**, Enthaltungen: **1**, Dagegen: **0** → **Er nimmt die Wahl an**
>
> Ulli: Dafür: **7**, Enthaltungen: **1**, Dagegen: **0** → **Er nimmt die Wahl an**
>
> Achim: Dafür: **6**, Enthaltungen: 2, Dagegen: **0** →  **Er nimmt die Wahl an**
>
> als Kassenprüfende.

## TOP 11: Beschluss des Protokolls

> Die Mitgliederversammlung beschließt das Protokoll dieser Mitgliederversammlung. Redaktionelle Änderungen des Protokolls können im Nachgang noch erfolgen, insbesondere zur Dokumentation und Verbesserung des Verständnisses und der Lesbarkeit.
>
> Das Protokoll wird im Anschluss sowohl als Text als auch als Druckdatei (PDF) öffentlich im Repository unter <https://codeberg.org/softwerke-magdeburg/Vereinsdokumente/> in einem Release bereitgestellt. Diese Datei wird im gleichen Repository unter dem Ordner Mitgliederversammlungen commited.
>
> Dafür: **8**, Enthaltungen: **0**, Dagegen: **0** → **Angenommen**

\newpage
\vspace*{5em}
\begin{tabular}{@{}p{.37\textwidth}p{.6\textwidth}@{}}
\\ \\
\hrulefill & \hrulefill \\
Ort, Datum & Sitzungsleitung: Till-Frederik Riechard
\end{tabular}

\vspace{5em}
\begin{tabular}{@{}p{.37\textwidth}p{.6\textwidth}@{}}
\\ \\
\hrulefill & \hrulefill \\
Ort, Datum & Protokoll: Phillipp Engelke
\end{tabular}